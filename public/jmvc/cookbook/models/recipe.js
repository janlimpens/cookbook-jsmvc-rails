steal('jquery/model', function() {

    /**
     * @class Cookbook.Models.Recipe
     * @parent index
     * @inherits jQuery.Model
     * Wraps backend recipe services.
     */
    $.Model('Cookbook.Models.Recipe',
        /* @Static */
        {
            findAll: "/recipes.json",
            findOne : "/recipes/{id}.json",
            create : function(attrs, success, error) {
                return $.post('/recipes.json', {recipe: attrs}, success)
            },
            update : "/recipes/{id}.json",
            destroy : function(id, success, error) {
                return $.ajax({
                    url: "/recipes/" + id + ".json",
                    type: 'DELETE',
                    dataType: 'json',
                    data: '',
                    success: success
                });
            }
        },
        /* @Prototype */
        {});
});